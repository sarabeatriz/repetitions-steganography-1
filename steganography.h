#ifndef STEGANOGRAPHY_H
#define STEGANOGRAPHY_H
#include <string>
#include <QImage>
#include <cmath>
#include <iostream>

using namespace std ;

class Steganography
{
public:
    Steganography();

};

//number of bits used to represent a letter
const int bitsPerLetter = 8;

/// \fn string decToBin(int num, int pad)
/// \~English
/// \brief Converts an decimal number to a string in base 2.
/// \param num a non-negative integer, the number that will be converted
/// \param pad a non-negative integer, the number of digits
///             in the resulting string
/// \return A string of length pad of 0's and/or 1's.
/// \~Spanish
/// \brief Convierte un numero entero a una cadena de caracteres
///                 en base 2.
/// \param num un entero positivo, el numero que se va a convertir.
/// \param pad un entero positivo, el numero de digitos en la cadena de
///             caracteres resultante.
/// \return Una cadena de caracteres de largo pad de 0's y/o 1's
///
string decToBin(int num, int pad) ;

/// \fn string messageToBinaryString(string str)
/// \~English
/// \brief Converts a string to a string of 1's and 0's
/// that represent the ASCII codes of the characters in the original string.
/// For example, "AB" results in "10000011000010".
///                               [-----][-----]
///                                  ^      ^
///                 ASCII code of A  |      |
///                 ASCII code of B --------+
///
/// \param str a string of characters to be converted to their ASCII representations
/// \return A string of 0's and/or 1's.
/// \~Spanish
/// \brief Convierte una caena de caracteres a una cadena de 1's y 0's
/// que representa el codigo ASCII de los caracteren en la cadena original.
/// Por ejemplo, "AB" resulta in "10000011000010".
///                               [-----][-----]
///                                  ^      ^
///              Codigo ASCII  de A  |      |
///              Codigo ASCII  de B --------+
/// \param str una cadena de caracteres a ser convertido a su representacion ASCII
/// \return Una cadena de 0's y/o 1's.
///
string messageToBinaryString(string str) ;

/// \fn char binStringToChar(string str)
/// \~English
/// \brief binStringToChar: Converts a string of 0's and 1's to a character
/// Example, with str="0011", returns the caracter 3.///
/// \param str: a string of 0's and 1's
/// \return A character whose binary representation is equivalent
///    to the string.
/// \~Spanish
/// \brief Convierte una cadena de 0's y 1's a un caracter.
/// Ejemplo, con str="0011", devuelve el caracter 3.
/// \param str una cadena de 0's y 1's
/// \return Un caracter ASCII cuya representacion binaria es equivalente a la cadena str.
///
char binStringToChar(string str) ;

/// \fn string binaryStringToMessage(string binStr)
/// \~English
/// \brief Converts a string of 0's and 1's to a string
///         of characters.
/// Example: with str="10000011000010", returns "AB", because
///          1000001 is ASCII of A, 1000010 is ASCII of B
/// \param str a string of 0's and 1's
/// \return A string that contains the characters whose ASCII codes
///    where specified in the input string.
/// \~Spanish
/// \brief Convierte una cadena de 0's y 1's a una
///         cadena de caracteres.
/// Ejemplo: con str = "10000011000010", devuelve "AB", porque
///          1000001 es ASCII de A, 1000010 es ASCII de B
/// \param str una cadena de 0's y 1's
/// \return  Una cadena que contiene los caracteres cuyos codigos ASCII
///     estaban especificados en la cadena de entrada.
///
string binaryStringToMessage(string binStr) ;

/// \fn int createMask(int lbits)
/// \~English
/// \brief Creates a mask of lbit 1's in the least significant positions.
/// Example, with lbits=4, returns the integer 15, which is mask 0x1111.
/// \param lbits a non-negative integer, the number of bits to set to one
/// \return An integer with lbit 1's in the least signicant positions.
/// \~Spanish
/// \brief Crea una mascara de lbit 1's en las posiciones menos significativas.
/// Ejemplo, con lbits=4, devuelve el entero 15, cuya mascara es 0x1111.
/// \param lbits un entero no negativo, el numero de bits a poner en uno.
/// \return Un entero con lbit 1's en las posiciones menos significativas.
///
int createMask(int lbits) ;

/// \fn EmbbedMessage(const QImage & origImage, QImage & newImage, string msg)
/// \~English
/// \brief Given an image (origImage) will create a new image (&newImage) by
/// embedding the bits of the msg into the least significant bits of
/// pixel colors of the original image.
/// \param origImage the original image will not be modified
/// \param newImage the image that is created
/// \param msg the string that will be embedded
/// \return By reference, the newImage is returned, containing the embedded message
/// \~Spanish
/// \brief Dada una imagen (origImage) creara una imagen nueva(&newImage)
/// empotrando los bits de el mensaje msg en los bits menos significativos de los
/// colores de la imagen original.
/// \param origImage la imagen original no va a ser modificada
/// \param newImage la imagen que se crea
/// \param msg la cadena que va a ser empotrada
/// \return Por referencia, es devuelta newImage, conteniendo el mensaje empotrado.
///
void EmbbedMessage(const QImage &origImage, QImage & newImage, string msg) ;

/// \fn string ExtractMessage(const QImage &stegoImage)
/// \~English
/// \brief Given an image (stegoImage) with the embedded message and the
/// the number of bits embedded into each pixels colors, will
/// extract the message that is hidden in the least significant
/// bits of the stegoImage.
/// \param stegoImage image that contains the embeded message
/// \return A string containing the extracted message.
/// \~Spanish
/// \brief Dada una imagen (stegoImage) con un mensaje embedido y el numero
/// numero de bits embedidios en cada color de los pixeles, va a extraer
/// el mensaje que esta escondido en los bits menos significativos de la stegoImage
/// \param stegoImage imagen que contiene el mensaje embedido
/// \return Una cadena conteniendo el mensaje extraido.
///
string ExtractMessage(const QImage &stegoImge) ;

#endif // STEGANOGRAPHY_H
