/********************************************************************************
** Form generated from reading UI file 'StegaPanel.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STEGAPANEL_H
#define UI_STEGAPANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StegaPanel
{
public:
    QWidget *centralWidget;
    QLabel *old_label;
    QLabel *new_label;
    QLabel *old_image;
    QLabel *new_image;
    QTextEdit *textEdit;
    QLabel *error_msg_label;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *loadImage;
    QPushButton *storeImage;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QSpinBox *bitBox;
    QPushButton *hideMessage;
    QPushButton *getMessage;
    QLabel *label_3;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *StegaPanel)
    {
        if (StegaPanel->objectName().isEmpty())
            StegaPanel->setObjectName(QStringLiteral("StegaPanel"));
        StegaPanel->resize(1055, 625);
        centralWidget = new QWidget(StegaPanel);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        old_label = new QLabel(centralWidget);
        old_label->setObjectName(QStringLiteral("old_label"));
        old_label->setGeometry(QRect(220, 10, 91, 21));
        new_label = new QLabel(centralWidget);
        new_label->setObjectName(QStringLiteral("new_label"));
        new_label->setGeometry(QRect(750, 10, 81, 16));
        old_image = new QLabel(centralWidget);
        old_image->setObjectName(QStringLiteral("old_image"));
        old_image->setGeometry(QRect(20, 40, 501, 401));
        old_image->setBaseSize(QSize(0, 0));
        old_image->setFrameShape(QFrame::StyledPanel);
        old_image->setScaledContents(true);
        new_image = new QLabel(centralWidget);
        new_image->setObjectName(QStringLiteral("new_image"));
        new_image->setGeometry(QRect(540, 40, 501, 401));
        new_image->setFrameShape(QFrame::StyledPanel);
        new_image->setScaledContents(true);
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(310, 460, 411, 91));
        error_msg_label = new QLabel(centralWidget);
        error_msg_label->setObjectName(QStringLiteral("error_msg_label"));
        error_msg_label->setGeometry(QRect(250, 571, 581, 20));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(77, 460, 171, 66));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        loadImage = new QPushButton(layoutWidget);
        loadImage->setObjectName(QStringLiteral("loadImage"));

        verticalLayout->addWidget(loadImage);

        storeImage = new QPushButton(layoutWidget);
        storeImage->setObjectName(QStringLiteral("storeImage"));

        verticalLayout->addWidget(storeImage);

        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(760, 460, 181, 106));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        bitBox = new QSpinBox(verticalLayoutWidget);
        bitBox->setObjectName(QStringLiteral("bitBox"));
        bitBox->setMinimum(1);
        bitBox->setMaximum(8);

        horizontalLayout_2->addWidget(bitBox);


        verticalLayout_3->addLayout(horizontalLayout_2);

        hideMessage = new QPushButton(verticalLayoutWidget);
        hideMessage->setObjectName(QStringLiteral("hideMessage"));

        verticalLayout_3->addWidget(hideMessage);

        getMessage = new QPushButton(verticalLayoutWidget);
        getMessage->setObjectName(QStringLiteral("getMessage"));

        verticalLayout_3->addWidget(getMessage);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(800, 550, 99, 29));
        QFont font;
        font.setPointSize(9);
        label_3->setFont(font);
        StegaPanel->setCentralWidget(centralWidget);
        layoutWidget->raise();
        old_label->raise();
        new_label->raise();
        old_image->raise();
        new_image->raise();
        textEdit->raise();
        error_msg_label->raise();
        verticalLayoutWidget->raise();
        label_3->raise();
        menuBar = new QMenuBar(StegaPanel);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1055, 22));
        StegaPanel->setMenuBar(menuBar);
        mainToolBar = new QToolBar(StegaPanel);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        StegaPanel->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(StegaPanel);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        StegaPanel->setStatusBar(statusBar);

        retranslateUi(StegaPanel);

        QMetaObject::connectSlotsByName(StegaPanel);
    } // setupUi

    void retranslateUi(QMainWindow *StegaPanel)
    {
        StegaPanel->setWindowTitle(QApplication::translate("StegaPanel", "Steganography", 0));
        old_label->setText(QApplication::translate("StegaPanel", "Original Image", 0));
        new_label->setText(QApplication::translate("StegaPanel", "Stego Image", 0));
        old_image->setText(QString());
        new_image->setText(QString());
        textEdit->setHtml(QApplication::translate("StegaPanel", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Lucida Grande'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", 0));
        error_msg_label->setText(QString());
        loadImage->setText(QApplication::translate("StegaPanel", "Load Image", 0));
        storeImage->setText(QApplication::translate("StegaPanel", "Store New Image", 0));
        label->setText(QApplication::translate("StegaPanel", "Number of Bits :", 0));
        hideMessage->setText(QApplication::translate("StegaPanel", "Hide Message", 0));
        getMessage->setText(QApplication::translate("StegaPanel", "Retrieve Message", 0));
        label_3->setText(QApplication::translate("StegaPanel", "(from Original Image)", 0));
    } // retranslateUi

};

namespace Ui {
    class StegaPanel: public Ui_StegaPanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STEGAPANEL_H
